
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//package edu.uci.ics.crawler4j.examples.basic;

import ir.tasks.twelve.a.Frequency;
import ir.tasks.twelve.a.Utilities;
import ir.tasks.twelve.a.WordComparator;
import ir.tasks.twelve.b.WordFrequencyCounter;
import ir.tasks.twelve.c.TwoGramFrequencyCounter;
import ir.tasks.twelve.d.PalindromeFrequencyCounter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class Crawler extends WebCrawler {

        private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4"
                        + "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
        
        List<PageStat> pagesStat;
        CrawlStat myCrawlStat;
        //List<String> uniqueUrls;
        
        List<Frequency> allpalindromes;
        
        public Crawler() {
                myCrawlStat = new CrawlStat();
                pagesStat = new ArrayList<PageStat>();
                //uniqueUrls = new ArrayList<String>();
                
                allpalindromes = new ArrayList<Frequency>();
        }

        /**
         * You should implement this function to specify whether the given url
         * should be crawled or not (based on your crawling logic).
         */
        @Override
        public boolean shouldVisit(WebURL url) {
                String href = url.getURL().toLowerCase();
                return !FILTERS.matcher(href).matches() && href.startsWith("http://www.flatricidepulgamitudepedia.org/");
        }

        /**
         * This function is called when a page is fetched and ready to be processed
         * by your program.
         */
		@Override
        public synchronized void visit(Page page) {
        		PageStat pageStat = new PageStat();	
	        	String url = page.getWebURL().getURL();
	        	//System.out.println("Visited: " + url);
	        	
	        	
                pageStat.setUrl(url);
	            myCrawlStat.incProcessedPages();
	            if (page.getParseData() instanceof HtmlParseData) {
	                    HtmlParseData parseData = (HtmlParseData) page.getParseData();
	                    String text = parseData.getText();
	                    String html = parseData.getHtml();
	                    List<WebURL> links = parseData.getOutgoingUrls();
	                    
	                    pageStat.setHtml(html);
                        pageStat.setNumberOfLinks(links.size());
                        pageStat.setText(text);
	                    
	                    myCrawlStat.incTotalLinks(links.size());
	                    List<Frequency> p = PalindromeFrequencyCounter.computePalindromeFrequencies(
	                    		Utilities.tokenizeFile(text));
	                    for(int b = 0; b < p.size(); b++){
	                    	allpalindromes.add(p.get(b));
	                    	//System.out.println(allpalindromes.get(b));
	                    	//System.out.println("Added " + p.get(b) + " to master palindrome list.");
	                    }
	                    p.clear();
	                    p = null;
	                    try {
	                            myCrawlStat.incTotalTextSize(text.getBytes("UTF-8").length);
	                    } catch (UnsupportedEncodingException ignored) {
	                            // Do nothing
	                    }
	            }
	            pagesStat.add(pageStat);
	            //uniqueUrls.add(pageStat.getUrl());
	           
                /*HtmlParseData parseData = (HtmlParseData) page.getParseData();
                String text = parseData.getText();
                StringTokenizer tokens = new StringTokenizer(text);
                ArrayList<String> words = new ArrayList<String>();
                //int i = 0;
                String regex = "[\\W ^_]";
                while(tokens.hasMoreTokens()){
                	//System.out.println(tokens.nextToken().toLowerCase().replaceAll(regex, " "));
                	words.add(tokens.nextToken().toLowerCase().replaceAll(regex, ""));
                    //System.out.println("added " + words.get(i) + " number " + i + " to words");
                    //i++;
                }*/

                
                
                //words.clear();
                //words = null;
                

                //System.out.println("Palindrome list size for this page: " + p.size());
               

	            // We dump this crawler statistics after processing every 50 pages
	            if (myCrawlStat.getTotalProcessedPages() % 30 == 0) {
	                    dumpMyData();
	            }
                
        }
        
        // This function is called by controller to get the local data of this
        // crawler when job is finished
        @Override
        public Object getMyLocalData() {
                return myCrawlStat;
        }
        
        /**
    	 * This function is called just before starting the crawl by this crawler
    	 * instance. It can be used for setting up the data structures or
    	 * initializations needed by this crawler instance.
    	 */
        @Override
        public void onStart() {

        }
        
        /**
    	 * This function is called just before the termination of the current
    	 * crawler instance. It can be used for persisting in-memory data or other
    	 * finalization tasks.
    	 */
        @Override
    	public void onBeforeExit() {
        	dumpMyData();
    	}
        
        //TODO IMPLEMENT IT BASED ON OUR REQUIREMENTS
        public void dumpMyData() {
            //int id = getMyId();
            
        	dumpStats();
            dumpUniquePages();
            dumpWords();
            dumpBigrams();
            dumpPalindromes();
            pagesStat.clear();
    }
        
        
    public void dumpStats() {
    	File partialResults = new File("partialResults.txt");
        try {
        	String longestPageUrl = null;
        	int longestPageLength = 0;
        	if (partialResults.exists()) {
        		BufferedReader input = new BufferedReader(new FileReader(partialResults));
        		String line;
        		int i = 1;
        		
        		
        		while ((line = input.readLine()) != null) {
        			//check if is the longest page line
        			if (i == 4) {
        				String[] parts = line.split(" ");
        				longestPageUrl = parts[0];
        				longestPageLength = Integer.parseInt(parts[1]);
        			}
        			i++;
        		}
        		input.close();
        	}
        	
        	for (PageStat page : pagesStat) {
        		if (page.getText().length() > longestPageLength) {
        			longestPageUrl = page.getUrl();
        			longestPageLength = page.getText().length();
        		}
        		
			}
        	
        	
			BufferedWriter output = new BufferedWriter(new FileWriter(partialResults));
			//partial time
			output.write( Long.toString( System.currentTimeMillis() ) + "\n");
			//System.out.println(Long.toString( System.currentTimeMillis() ) + "\n");
			//partial total processed pages
			output.write(Integer.toString(myCrawlStat.getTotalProcessedPages() ) + "\n");
			//partial total links
			output.write( Long.toString( myCrawlStat.getTotalLinks() ) + "\n" );
			//partial longest page
			output.write( longestPageUrl + " " + Long.toString( longestPageLength ) + "\n" );
			
			output.close();
			
			//pages that has the bigram "flatricide pulgamitude"

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
        
        
    public void dumpUniquePages() {
    	File uniquePages = new File("urls.txt");
		
    	try {
    		
			BufferedWriter outputUP = new BufferedWriter(new FileWriter(uniquePages, true));
			
			
			for (PageStat page : pagesStat) {
				//partial unique pages
        		outputUP.write( page.getUrl() + "\n" );
			}
			
			outputUP.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
        
        
    public void dumpBigrams() {
    	//partial 25 most common bigrams
		for (PageStat page:pagesStat)
		{
			List<String> pageWords = new ArrayList<String> (page.getWordList());
			List<String> stopWordsList;
			try {
				
				stopWordsList = new ArrayList<String> (Utilities.inputStopWordsList());
				List<String> wordFrequencyList = new ArrayList<String>(); 
				for (String word : pageWords)
				{
					if (stopWordsList.contains(word))
					{
						//System.out.println("STOP WORD: " + word);
					}
					else 
					{
						wordFrequencyList.add(word);
					}
				}
				List<Frequency> pageFreqs = TwoGramFrequencyCounter.computeTwoGramFrequencies(wordFrequencyList); 
				page.setPageFreq(pageFreqs);
				//Sort alphabetically the words in page 
				Collections.sort(pageFreqs, new WordComparator());
				//GET textfile values to a list 
				//	List <Frequency> textFileValues = new ArrayList <Frequency> (Utilities.getTextFile(word));
				TwoGramFrequencyCounter.biGramstoreToProperTxtFiles(pageFreqs);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
    }
    
    public void dumpWords() {
    	//partial 25 most common words
		//Create the files for each alphabet
		//	Utilities.createWordFiles(); --NOT needed -> creates a file when storing to proper text files 
		
		//FINDS FREQ LIST OF EACH PAGE
		for (PageStat page:pagesStat)
		{
			List<String> pageWords = new ArrayList<String> (page.getWordList());
			List<String> stopWordsList;
			try {
				stopWordsList = new ArrayList<String> (Utilities.inputStopWordsList());
				List<String> wordFrequencyList = new ArrayList<String>(); 
				for (String word : pageWords)
				{
					if (stopWordsList.contains(word))
					{
						//System.out.println("STOP WORD: " + word);
					}
					else 
					{
						wordFrequencyList.add(word);
					}
				}
				List<Frequency> pageFreqs = WordFrequencyCounter.computeWordFrequencies(wordFrequencyList); 
				page.setPageFreq(pageFreqs);
				
				//Sort alphabetically the words in page 
				Collections.sort(pageFreqs, new WordComparator());
				//GET textfile values to a list 
				//	List <Frequency> textFileValues = new ArrayList <Frequency> (Utilities.getTextFile(word));
				Utilities.storeToProperTxtFiles(pageFreqs);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
    }
        
        
    public void dumpPalindromes() {
    	//partial 10 longest palindromes
		//he said to dump all palindromes and we can manually select top 10 to put in PDF
		File finalFreqFile = new File("palindromes.txt");
		
		//Collections.sort(allpalindromes, new FrequencyComparator());
		//System.out.println("Total Palindrome list size: " + allpalindromes.size());
		//for(int i = 0; i < 20; i++){
		
		try{
			BufferedWriter pal = new BufferedWriter(new FileWriter(finalFreqFile, true));
			int i = 0;
			while(i < allpalindromes.size() 
					//&& i < 20
					) {
				pal.write(allpalindromes.get(i).getText() + ":" + allpalindromes.get(i).getFrequency() + " ");
			//System.out.println("Palindrome " + i + ": " + allpalindromes.get(i).getText() + ", " + allpalindromes.get(i).getFrequency());
				i++;
	        }
			pal.close();
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		allpalindromes.clear();
    }
}
