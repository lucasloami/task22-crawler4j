//CS 121 Task 12
//Melody Truong (#28504378)
//Christopher Wong (#28264457)

package ir.tasks.twelve.c;

import ir.tasks.twelve.a.Frequency;
import ir.tasks.twelve.a.Utilities;
import ir.tasks.twelve.b.FrequencyComparator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * Count the total number of 2-grams and their frequencies in a text file.
 */
public final class TwoGramFrequencyCounter {
	/**
	 * This class should not be instantiated.
	 */
	private TwoGramFrequencyCounter() {}

	/**
	 * Takes the input list of words and processes it, returning a list
	 * of {@link Frequency}s.
	 * 
	 * This method expects a list of lowercase alphanumeric strings.
	 * If the input list is null, an empty list is returned.
	 * 
	 * There is one frequency in the output list for every 
	 * unique 2-gram in the original list. The frequency of each 2-grams
	 * is equal to the number of times that two-gram occurs in the original list. 
	 * 
	 * The returned list is ordered by decreasing frequency, with tied 2-grams sorted
	 * alphabetically. 
	 * 
	 * 
	 * 
	 * Example:
	 * 
	 * Given the input list of strings 
	 * ["you", "think", "you", "know", "how", "you", "think"]
	 * 
	 * The output list of 2-gram frequencies should be 
	 * ["you think:2", "how you:1", "know how:1", "think you:1", "you know:1"]
	 *  
	 * @param words A list of words.
	 * @return A list of two gram frequencies, ordered by decreasing frequency.
	 */
	public static List<Frequency> computeTwoGramFrequencies(List<String> words) {
		ArrayList<Frequency> result = new ArrayList<Frequency>();
		//If list of words is null, return an empty list
		if (words == null)
			return result;
		//Create map -> key is word, value is its frequency
		TreeMap<String,Frequency> map = new TreeMap<String,Frequency>();
		//Make pointers so that you can form 2-gram words
		int i=0;
		int j=1;
		while (j<words.size())
		{
			//Get first word from pointer 1
			String t1 = words.get(i);
			//Get second word from pointer 2
			String t2 = words.get(j);
			//Combine first and second word to form 2-gram word
			String word = t1 + " " + t2;
			//Word exists in map
			if (map.containsKey(word))
				//Grab the word from the map and update its value (i.e. increase its frequency)
				map.get(word).incrementFrequency();
			//Word does not exist in map
			else
				//Add word to map and give it a frequency of 1
				map.put(word, new Frequency(word, 1));
			//Increment pointers
			i++;
			j++;
		}
		//Put all Frequency objects into a list
		result.addAll(map.values());
		//Sort the list of Frequency objects
		Collections.sort(result, new FrequencyComparator());
		return result;
	}
	
	
	public static void biGramstoreToProperTxtFiles(List<Frequency> pageFreqs) throws FileNotFoundException
	{
		BufferedWriter br;
		for (Frequency freq: pageFreqs)
		{
			String word = freq.getText();
			char letter = word.toLowerCase().charAt(0); 
			
			//Put all Freq
			
			//WriteFile out = new WriteFile(letter + "biGram.txt", true); 
			try {
				br = new BufferedWriter(new FileWriter(letter + "biGram.txt", true));
				br.write(freq.getText() + " " + freq.getFrequency()+"\n");
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("NOT PRINTING TO " + letter);
				e.printStackTrace();
			}; 
		}
	}

	/**
	 * Runs the 2-gram counter. The input should be the path to a text file.
	 * 
	 * @param args The first element should contain the path to a text file.
	 */
	public static void main(String[] args) {
		File file = new File(args[0]);
		ArrayList<String> words = Utilities.tokenizeFile(file);
		List<Frequency> frequencies = computeTwoGramFrequencies(words);
		Utilities.printFrequencies(frequencies);
	}
}
