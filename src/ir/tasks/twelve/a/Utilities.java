//CS 121 Task 12
//Melody Truong (#28504378)
//Christopher Wong (#28264457)

package ir.tasks.twelve.a;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * A collection of utility methods for text processing.
 */
public class Utilities {
	/**
	 * Reads the input text file and splits it into alphanumeric tokens.
	 * Returns an ArrayList of these tokens, ordered according to their
	 * occurrence in the original text file.
	 * 
	 * Non-alphanumeric characters delineate tokens, and are discarded.
	 *
	 * Words are also normalized to lower case. 
	 * 
	 * Example:
	 * 
	 * Given this input string
	 * "An input string, this is! (or is it?)"
	 * 
	 * The output list of strings should be
	 * ["an", "input", "string", "this", "is", "or", "is", "it"]
	 * 
	 * @param input The file to read in and tokenize.
	 * @return The list of tokens (words) from the input file, ordered by occurrence.
	 */
	public static ArrayList<String> tokenizeFile(File input) {
		ArrayList<String> arrayList = new ArrayList<String>();
		String regex = "[\\W ^_]";
        try {
            Reader reader = new InputStreamReader(new FileInputStream(input),"UTF-8");
            BufferedReader fileIn = new BufferedReader(reader);
            String line;
            while ((line = fileIn.readLine())!=null) {
            	line = line.replaceAll(regex, " ");
            	//Grab line from file
            	StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreTokens()){
                	//Grab word from line
                	String token = st.nextToken().toLowerCase();
                	//Add word to list
                	arrayList.add(token);
                }
            }
            fileIn.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
	}
	
	public static List<String> tokenizeFile(String text)
	{
		//String regex = "\\W + \\s*|\\s+\\W+|\\_+";  
	
	
		List <String> strList = new ArrayList<String>(Arrays.asList(text.toLowerCase().split("\\W+\\s*|\\s+\\W+|\\_+")));
		
		return strList; 
	}

	/**
	 * Takes a list of {@link Frequency}s and prints it to standard out. It also
	 * prints out the total number of items, and the total number of unique items.
	 * 
	 * Example one:
	 * 
	 * Given the input list of word frequencies
	 * ["sentence:2", "the:1", "this:1", "repeats:1",  "word:1"]
	 * 
	 * The following should be printed to standard out
	 * 
	 * Total item count: 6
	 * Unique item count: 5
	 * 
	 * sentence	2
	 * the		1
	 * this		1
	 * repeats	1
	 * word		1
	 * 
	 * 
	 * Example two:
	 * 
	 * Given the input list of 2-gram frequencies
	 * ["you think:2", "how you:1", "know how:1", "think you:1", "you know:1"]
	 * 
	 * The following should be printed to standard out
	 * 
	 * Total item count: 6
	 * Unique item count: 5
	 * 
	 * you think	2
	 * how you		1
	 * know how		1
	 * think you	1
	 * you know		1
	 * 
	 * @param frequencies A list of frequencies.
	 */
	public static void printFrequencies(List<Frequency> frequencies) {
		//Unique item count is equal to the number of Frequency objects in the list
		int uniqueItemCount = frequencies.size();
		String itemOutput ="";
		int totalItemCount = 0;
		itemOutput += "\nUnique item count: " + uniqueItemCount + "\n";
		for (Frequency frequency : frequencies){
			//Update totalItemCount
			totalItemCount += frequency.getFrequency();
			//Append word and frequency to output string
			itemOutput += "\n" + frequency.getText() + "\t" + String.valueOf(frequency.getFrequency());
		}
		System.out.println("Total item count: " + totalItemCount + itemOutput);
	}
	
	//Creates a List of Stopwords
	public static List<String> inputStopWordsList() throws FileNotFoundException
	{
		List<String> stopWordsList = new ArrayList<String>(); 
		
		File stopWordsFile = new File("stgFolder/stopWords.txt"); 
		Scanner scanner = new Scanner (stopWordsFile);
		while (scanner.hasNextLine())
		{
			String word = scanner.nextLine(); 
			stopWordsList.add(word); 
		}
		scanner.close(); 
		return stopWordsList;
	}
	
	//Checks if word is on stopWordsList
	public static boolean isStopWord(List<String> stopWordsList, String word)
	{
		if (stopWordsList.contains(word))
		{
			return true; 
		}
		else
		{
			return false;
		}
	}
	
	
	
	public static void createWordFiles () throws IOException
	{
		String[] alphabet = {"a", "b","c", "d", "e", "f", "g", "h", "i", "j", "k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
		//CREATES all the files 
	
		for (String alpha: alphabet)
		{
			File wordFile = new File(alpha +".txt");
			wordFile.createNewFile();
			//BufferedWriter output = new BufferedWriter(new FileWriter(wordFile));
		}
	}
	
	public static void storeToProperTxtFiles(List<Frequency> pageFreqs) throws FileNotFoundException
	{
		BufferedWriter br;
		for (Frequency freq: pageFreqs)
		{
			String word = freq.getText();
			char letter = word.toLowerCase().charAt(0); 
			
			//Put all Freq
			
			//WriteFile out = new WriteFile(letter + ".txt", true); 
			try {

				br = new BufferedWriter(new FileWriter(letter + ".txt", true));
				br.write(freq.getText() + " " + freq.getFrequency()+"\n");
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("NOT PRINTING TO " + letter);
				e.printStackTrace();
			}; 
		}
	}

	
	public static List<Frequency> getTextFile(String word) throws FileNotFoundException
	{
		char letter = word.charAt(0); 
		File fileName = new File(letter + ".txt"); 

		Scanner input = new Scanner (fileName);
		List <Frequency> textFileValues = new ArrayList <Frequency> (); 
		while (input.hasNext())

		{
			String str = input.next(); 
			int freq = input.nextInt(); 
			
			Frequency addFreq = new Frequency(str, freq); 
			textFileValues.add(addFreq); 
		}
		input.close();
		return textFileValues; 
	}
	
	public static Map<String, Frequency> getTextFileMap(String word) throws FileNotFoundException
	{
		char letter = word.charAt(0); 
		File fileName = new File(letter + ".txt"); 

		Scanner input = new Scanner (fileName);
	//	List <Frequency> textFileValues = new ArrayList <Frequency> (); 
		
		Map<String, Frequency> textFileMap = new TreeMap <String, Frequency> (); 
		while (input.hasNext())
		{
			String str = input.next(); 
			int freq = input.nextInt(); 
			
			Frequency addFreq = new Frequency(str, freq); 
			//textFileValues.add(addFreq); 
			textFileMap.put(str, addFreq); 
			
		}
		input.close();
		//return textFileValues; 
		return textFileMap; 
	}
	
	public static List<Frequency> checkTextFileDuplicate(List<Frequency> pageFreq, String word) throws FileNotFoundException
	{
		//gets textFile values to map
		Map<String, Frequency> textFileMap = new TreeMap <String, Frequency> (Utilities.getTextFileMap(word));

		//check page if words already on text file
		for (Frequency freq: pageFreq)
		{
			if (textFileMap.containsKey(freq.getText()))
			{
				textFileMap.get(freq.getText()).incrementFrequency();
			}
			else 
			{
				textFileMap.put(freq.getText(), freq); 
			}
		}
		
		List<Frequency> textFileToOverwriteWith = new ArrayList<Frequency>(textFileMap.values()); 
		return textFileToOverwriteWith; 
	}
}
