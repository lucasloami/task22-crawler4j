package ir.tasks.twelve.a;

import java.util.Comparator;

public class WordComparator implements Comparator<Frequency> 
{
	@Override
	public int compare(Frequency f1, Frequency f2) 
	{
		return f1.getText().compareTo(f2.getText());
	}
}