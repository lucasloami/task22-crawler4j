//CS 121 Task 12
//Melody Truong (#28504378)
//Christopher Wong (#28264457)

package ir.tasks.twelve.d;

import ir.tasks.twelve.a.Frequency;
import ir.tasks.twelve.a.Utilities;
import ir.tasks.twelve.b.FrequencyComparator;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class PalindromeFrequencyCounter {
	/**
	 * This class should not be instantiated.
	 */
	private PalindromeFrequencyCounter() {}

	/**
	 * Takes the input list of words and processes it, returning a list
	 * of {@link Frequency}s.
	 * 
	 * This method expects a list of lowercase alphanumeric strings.
	 * If the input list is null, an empty list is returned.
	 * 
	 * There is one frequency in the output list for every 
	 * unique palindrome found in the original list. The frequency of each palindrome
	 * is equal to the number of times that palindrome occurs in the original list. Smaller palindromes
	 * within larger palindromes should not be separately counted.  Overlapping
	 * palindromes should be counted separately.
	 * 
	 * Palindromes can span sequential words in the input list.
	 * 
	 * The returned list is ordered by decreasing frequency, with tied palindromes sorted
	 * alphabetically. 
	 * 
	 * The original list is not modified.
	 *
	 * In a literary sense, palindromes end on word boundaries.  For our
	 * purposes palindromes can start and end within words.
	 *
	 * Only return palindromes longer than 5 characters.  Spaces do not count
	 * and should not be included in the returned list.
	 * 
	 * Example:
	 * 
	 * Given the input list of strings 
	 * ["do", "geese", "see", "god", "abba", "bat", "tab","xxbap","bat", "tab"]
	 * 
	 * The output list of palindromes should be 
	 * [ "battab:2", "abxxba:1", "dogeeseseegod:1"]
	 *  
	 * @param words A list of words.
	 * @return A list of palindrome frequencies, ordered by decreasing frequency.
	 */
	public static List<Frequency> computePalindromeFrequencies(List<String> words) {
		ArrayList<Frequency> result = new ArrayList<Frequency>();
		//If list of words is null, return an empty list
		if (words == null)
			return result;
		//Create map -> key is word, value is its frequency
		TreeMap<String,Frequency> map = new TreeMap<String,Frequency>();
		//Combine all words into a string buffer
		StringBuffer sb = new StringBuffer();
		for (String word: words)
			sb.append(word);
		//Since palindromes need to be >= 5 characters, we can start at index 4
		int current = 4;
		while (current < sb.length()-1)
		{
			//Create pointers for characters before and after the current character
			int back = current-1;
			int forward = current+1;
			//Example of oddPalindrome: "dabad"
			boolean oddPalindrome = sb.charAt(back)==sb.charAt(forward);
			//Example of evenPalindrome: "dbaabd"
			boolean evenPalindrome = !oddPalindrome && sb.charAt(current)==sb.charAt(forward);
			boolean palindrome = oddPalindrome || evenPalindrome;
			if (palindrome)
			{
				//If palindrome is an evenPalindrome, then we need to increment forward pointer
				if (evenPalindrome)
					forward++;
				//Expand the palindrome to get the longest palindrome possible
				while (back>=0 && forward<sb.length() && sb.charAt(back)==sb.charAt(forward))
				{
					back--;
					forward++;
				}
				//Grab longest palindrome
				String word = sb.substring(back+1, forward);
				//Assert that the palindrome is <= 5 characters
				if (word.length()>=5)
				{
					//Palindrome exists in map
					if (map.containsKey(word))
						//Grab the palindrome from the map and update its value (i.e. increase its frequency)
						map.get(word).incrementFrequency();
					//Palindrome does not exist in map
					else
						//Add palindrome to map and give it a frequency of 1
						map.put(word, new Frequency(word, 1));
				}
			}
			current++;
		}
		//Put all Frequency objects into a list
		result.addAll(map.values());
		//Sort the list of Freqency objects
		Collections.sort(result, new FrequencyComparator());
		return result;
	}

	/**
	 * Same as computePalindromeFrequencies except it only includes palindromes
	 * that end on word boundaries.
	 * 
	 * Example:
	 * 
	 * Given the input list of strings 
	 * ["do", "geese", "see", "god", "abba", "bat", "tab","xxbap","bat", "tab"]
	 * 
	 * The output list of palindromes should be 
	 * [ "battab:2", "dogeeseseegod:1"]
	 *  
	 * @param words A list of words.
	 * @return A list of palindrome frequencies, ordered by decreasing frequency.
	 */

	//This method checks to see if a palindrome ends with a word in the list of words
	//Example: endsWithWord("dabad", ["bad", "good", "right", "wrong"])
	//-->This example would return true because "dabad" ends with "bad"
	public static boolean endsWithWord(String palindrome, List<String> words)
	{
		//Put the palindrome in a string buffer to compare the end characters
		StringBuffer sb = new StringBuffer(palindrome);
		for (int i=sb.length()-1; i>=0; i--)
		{
			//Find word combination that the palindrome ends with
			//Examples of words that "dabad" ends with: "bad", "ad", "d"
			String word = sb.substring(i, sb.length());
			//If the list of words contain the word comination, then return true
			if (words.contains(word))
				return true;
		}
		return false;
	}

	public static List<Frequency> computeWordAwarePalindromeFrequencies(List<String> words) {
		ArrayList<Frequency> result = new ArrayList<Frequency>();
		//If list of words is null, return an empty list
		if (words == null)
			return result;
		//Create map -> key is word, value is its frequency
		TreeMap<String,Frequency> map = new TreeMap<String,Frequency>();
		//Combine all words into a string buffer
		StringBuffer sb = new StringBuffer();
		for (String word: words)
			sb.append(word);
		//Since palindromes need to be >= 5 characters, we can start at index 4
		int current = 4;
		while (current < sb.length()-1)
		{
			//Create pointers for characters before and after the current character
			int back = current-1;
			int forward = current+1;
			//Example of oddPalindrome: "dabad"
			boolean oddPalindrome = sb.charAt(back)==sb.charAt(forward);
			//Example of evenPalindrome: "dbaabd"
			boolean evenPalindrome = !oddPalindrome && sb.charAt(current)==sb.charAt(forward);
			boolean palindrome = oddPalindrome || evenPalindrome;
			if (palindrome)
			{
				//If palindrome is an evenPalindrome, then we need to increment forward pointer
				if (evenPalindrome)
					forward++;
				//Expand the palindrome to get the longest palindrome possible
				while (back>=0 && forward<sb.length() && sb.charAt(back)==sb.charAt(forward))
				{
					back--;
					forward++;
				}
				//Grab longest palindrome
				String word = sb.substring(back+1, forward);
				//Assert that the palindrome is <= 5 characters
				if (word.length()>=5)
				{
					//Palindrome exists in map
					if (map.containsKey(word))
						//Grab the palindrome from the map and update its value (i.e. increase its frequency)
						map.get(word).incrementFrequency();
					//Palindrome does not exist in map
					else if (endsWithWord(word, words))
						//Add palindrome to map and give it a frequency of 1
						map.put(word, new Frequency(word, 1));
				}
			}
			current++;
		}
		//Put all Frequency objects into a list
		result.addAll(map.values());
		//Sort the list of Frequency objects
		Collections.sort(result, new FrequencyComparator());
		return result;
	}

	/**
	 * Runs the palindrome finder. The input should be the path to a text file.
	 * 
	 * @param args The first element should contain the path to a text file.
	 */
	public static void main(String[] args) {
		File file = new File(args[0]);
		ArrayList<String> words = Utilities.tokenizeFile(file);
		List<Frequency> frequencies = computeWordAwarePalindromeFrequencies(words);
		Utilities.printFrequencies(frequencies);
	}
}
