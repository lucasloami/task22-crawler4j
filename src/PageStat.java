import ir.tasks.twelve.a.Frequency;
import ir.tasks.twelve.a.Utilities;

import java.util.List;


public class PageStat {
	private String url;
	private String text; //entire text for the page 
	private String html;
	private int numberOfLinks;
	private List<Frequency> pageFreq; 
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public int getNumberOfLinks() {
		return numberOfLinks;
	}
	public void setNumberOfLinks(int numberOfLinks) {
		this.numberOfLinks = numberOfLinks;
	}
	
	public void setPageFreq(List<Frequency> pageFreq)
	{
		this.pageFreq = pageFreq; 
	}
	

	public List<String> getWordList ()
	{
		List <String> wordList = Utilities.tokenizeFile(text); 
		
		return wordList; 
	}
	
	public List<Frequency> getPageFreq()
	{
		return pageFreq; 
	}
	
}
